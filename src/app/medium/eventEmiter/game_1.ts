import {EventEmitter} from '@angular/core';

export class Game2 {
  pointLive: number;
  pointChange = new EventEmitter<number>();

  constructor() {
    this.pointLive = 100;
  }

  gamerOver(fail: number): void {
    if (fail >= this.pointLive) {
      this.pointLive = 0;
    } else {
      this.pointLive = this.pointLive - fail;
    }
    this.pointChange.emit(this.pointLive);
  }
}
