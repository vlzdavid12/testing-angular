import {Game2} from './game_1';

describe('Game 2 emit', () => {
  let game: Game2;

  beforeEach(() => {
    game = new Game2();
  });


  it('Execute in event when yuo receive fails ', () => {
    let newHP = 0;

    game.pointChange.subscribe(hp => {
      newHP = hp;
    });

    game.gamerOver(1000);

    expect(newHP).toBe(0);
  });

  it('Execute in event when yuo receive fails is -100', () => {
    let newHP = 0;

    game.pointChange.subscribe(hp => newHP = hp);

    game.gamerOver(50);

    expect(newHP).toBe(50);
  });

});
