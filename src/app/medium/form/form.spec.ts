import {FormLogin} from './form';
import {FormBuilder} from '@angular/forms';

describe('Form Login Validation', () => {
  let component: FormLogin;

  beforeEach(() => {
    component = new FormLogin(new FormBuilder());
  });

  it('Have form create 2 inputs, email and password', () => {
    expect(component.form.contains('email')).toBeTruthy();
    expect(component.form.contains('password')).toBeTruthy();
  });

  it('Email have input required', () => {
    const control = component.form.get('email');
    control?.setValue('');
    expect(control?.valid).toBeFalsy();
  });

  it('Email valid!', () => {
    const control = component.form.get('email');
    control?.setValue('vlzdavid12@gmail.com');
    expect(control?.valid).toBeTruthy();
  });

});
