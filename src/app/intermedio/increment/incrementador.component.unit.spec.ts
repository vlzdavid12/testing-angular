import {IncrementadorComponent} from './incrementador.component';

describe('Increment Component Unit', () => {
  let component: IncrementadorComponent;
  beforeEach(() =>  component = new IncrementadorComponent())

  it('The progress bar should not pass more than 100', () => {
    component.progreso = 50;
    component.cambiarValor(5);
    expect(component.progreso).toBe(55);
  });
});
