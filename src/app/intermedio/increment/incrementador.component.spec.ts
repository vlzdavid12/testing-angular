import {TestBed, ComponentFixture} from '@angular/core/testing';
import {IncrementadorComponent} from './incrementador.component';
import {FormsModule} from '@angular/forms';
import {By} from '@angular/platform-browser';


describe('Increment Component', () => {

  let component: IncrementadorComponent;
  let fixture: ComponentFixture<IncrementadorComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [IncrementadorComponent],
      imports: [FormsModule]
    });

    fixture = TestBed.createComponent(IncrementadorComponent);
    component = fixture.componentInstance;

  });

  it('Have show legend progress', () => {

    component.leyenda = 'Progress Loader';
    // start effects change
    fixture.detectChanges();
    const element: HTMLElement = fixture.debugElement.query(By.css('h3')).nativeElement;
    expect(element.innerHTML).toContain('Progress Loader');

  });

  it('Have show in the input of value of progress', () => {
    component.cambiarValor(5);
    // start effects change
    fixture.detectChanges();

    fixture.whenStable().then(() => {
      const input = fixture.debugElement.query(By.css('input'));
      const element = input.nativeElement;

      expect(element.value).toBe('55');
    });
  });

  it('Have decrement / increment in 5, click button action.', () => {
    const buttons = fixture.debugElement.queryAll(By.css('.btn-primary'));
    // Click 1
    buttons[0].triggerEventHandler('click', null);
    expect(component.progreso).toBe(45);

    // Click 2
    buttons[1].triggerEventHandler('click', null);
    expect(component.progreso).toBe(50);
  });

  it('Title component have text progress', () => {
    const buttons = fixture.debugElement.queryAll(By.css('.btn-primary'));
    buttons[0].triggerEventHandler('click', null);

    fixture.detectChanges();

    const element: HTMLElement = fixture.debugElement.query(By.css('h3')).nativeElement;
    expect(element.innerHTML).toContain('45');
  });

});
