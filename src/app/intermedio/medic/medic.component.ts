import { Component, OnInit } from '@angular/core';
import {MedicService} from './medic.service';

@Component({
  selector: 'app-medic',
  templateUrl: './medic.component.html',
  styles: [
  ]
})
export class MedicComponent implements OnInit {

  medics: any[] = [];

  constructor(private medicService: MedicService) {}

  ngOnInit(): void {
    this.getMedic('Alejandro');
  }

  getMedic(name: string): string{
    return `Hello mr. ${name}`;
  }

  getMedics(): void{
    this.medicService.getMedics()
      .subscribe((medics: any) => this.medics = medics);
  }

}
