import {TestBed, ComponentFixture} from '@angular/core/testing';
import {MedicComponent} from './medic.component';
import {MedicService} from './medic.service';
import {HttpClientModule} from '@angular/common/http';

describe('MedicComponent', () => {
  let component: MedicComponent;
  let fixture: ComponentFixture<MedicComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [MedicComponent],
      providers: [MedicService],
      imports: [HttpClientModule]
    });
    fixture = TestBed.createComponent(MedicComponent);
    component = fixture.componentInstance;

  });

  it('Have create of component MedicComponent', () => {
    expect(component).toBeTruthy();
  });

  it('Have return name of medic', () => {
    const name = 'ALEJANDRO';
    const resp = component.getMedic(name);
    expect(resp).toContain(name)
  });




});
