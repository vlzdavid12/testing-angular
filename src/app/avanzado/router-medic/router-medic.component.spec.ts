import {ComponentFixture, TestBed} from '@angular/core/testing';

import {RouterMedicComponent} from './router-medic.component';
import {ActivatedRoute, Router} from '@angular/router';
import {Observable, EMPTY, Subject} from 'rxjs';


class FakeRouter {
  navigate(params: any): void {

  };
}

class FakeActivateRouter {
  // params: Observable<any> = EMPTY;
  private subject = new Subject();

  push(value: any): void {
    this.subject.next(value);
  }

  get params(): any {
    return this.subject.asObservable();
  }

}


describe('RouterMedicComponent', () => {
  let component: RouterMedicComponent;
  let fixture: ComponentFixture<RouterMedicComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RouterMedicComponent],
      providers: [
        {provide: Router, useClass: FakeRouter},
        {provide: ActivatedRoute, useClass: FakeActivateRouter}]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RouterMedicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('Have redirect medic when save. ', () => {
    const router = TestBed.get(Router);
    const spy = spyOn(router, 'navigate');

    component.saveMedic();

    expect(spy).toHaveBeenCalledWith(['medico', '123']);
  });


  it('Have ID in New', () => {
    component = fixture.componentInstance;
    const activateRouter: FakeActivateRouter = TestBed.get(ActivatedRoute);
    activateRouter.push({id: 'New'});

    expect(component.id).toBe('New');

  });

});
