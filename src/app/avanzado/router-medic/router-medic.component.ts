import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-router-medic',
  templateUrl: './router-medic.component.html',
  styles: []
})
export class RouterMedicComponent implements OnInit {

  id: string = '';

  constructor(
    public router: Router,
    public activatedRoute: ActivatedRoute
  ) {
  }

  ngOnInit(): void {
    this.activatedRoute.params
      .subscribe(params => {
        this.id = params['id'];
      });
  }

  saveMedic(): void {
    this.router.navigate(['medico', '123']);
  }


}
