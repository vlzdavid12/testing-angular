import {Rutas} from './routes.component';
import {MedicComponent} from '../../intermedio/medic/medic.component';


describe('Rutas First Validate', () => {
  it('Exist router path medic/id', () => {
    expect(Rutas).toContain(
      {
        path: 'medic/id',
        component: MedicComponent
      }
    );
  });


});
