import {Routes} from '@angular/router';
import {HospitalComponent} from '../../intermedio/hospital/hospital.component';
import {MedicComponent} from '../../intermedio/medic/medic.component';
import {IncrementadorComponent} from '../../intermedio/increment/incrementador.component';

export const Rutas: Routes = [
  { path: '', component: IncrementadorComponent},
  { path: 'hospital', component: HospitalComponent},
  { path: 'medic/id', component: MedicComponent},
  { path: '**', redirectTo: ''}
]
