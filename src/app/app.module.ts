import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {HttpClientModule} from '@angular/common/http';
import { MedicComponent } from './intermedio/medic/medic.component';
import { HospitalComponent } from './intermedio/hospital/hospital.component';
import {IncrementadorComponent} from './intermedio/increment/incrementador.component';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {Rutas} from './avanzado/routes/routes.component';
import { NavbarComponent } from './avanzado/navbar/navbar.component';
import { RouterMedicComponent } from './avanzado/router-medic/router-medic.component';


@NgModule({
  declarations: [
    AppComponent,
    MedicComponent,
    HospitalComponent,
    IncrementadorComponent,
    NavbarComponent,
    RouterMedicComponent
  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        FormsModule,
        RouterModule.forRoot(Rutas)
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
