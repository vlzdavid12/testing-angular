import {Game} from './clases';

describe('Validate function class', () => {
  let game = new Game();

  // Circles Live Proof
  beforeAll(() => {
    console.log('eject beforeAll');
  });
  beforeEach(() => {
    console.log('eject beforeEach');
    game =  new Game();
  });
  afterAll(() => {
    console.log('eject afterAll');
  });
  afterEach(() => {
    console.log('eject afterEach');
  });

  it('Return 80 to receive 20 fail', () => {
    // const game =  new Game();
    const resp = game.gamerOver(20);
    expect(resp).toBe(80);
  });

  it('Return 50 to receive 50 fail', () => {
    // const game =  new Game();
    const resp = game.gamerOver(50);
    expect(resp).toBe(50);
  });

  it('Return 0 to receive 100 fail or more', () => {
    // const game =  new Game();
    const resp = game.gamerOver(100);
    expect(resp).toBe(0);
  });
});
