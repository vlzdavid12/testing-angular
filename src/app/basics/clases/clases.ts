export class Game {
  pointLive: number;

  constructor() {
    this.pointLive = 100;
  }

  gamerOver(fail: number): number {
    if (fail >= this.pointLive) {
      this.pointLive = 0;
    } else {
      this.pointLive = this.pointLive - fail;
    }
    return this.pointLive;
  }
}
