import {getHero} from './arrays';

xdescribe('Validate array of SuperHero', () => {
  it('SuperHero is array, must number 3', () => {
    const resp =  getHero();
    expect(resp.length).toBeGreaterThanOrEqual(3);
  });
  it('SuperHero exist string: Robocot, Megaman', () => {
    const resp =  getHero();
    expect(resp).toContain('Megaman');
    expect(resp).toContain('Robocot');
  });
});
