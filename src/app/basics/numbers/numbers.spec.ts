import {increment} from './numbers';

describe('Validate is number', () => {
  it('Number toBe must 100', () => {
    const res = increment(300);
    expect(res).toBe(100);
  });
  it('Number return +1 with 50', () => {
    const res = increment(50);
    expect(res).toBe(51);
  });
});
