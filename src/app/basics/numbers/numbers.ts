export function increment(inputNumber: number): number{
  if (inputNumber > 100){
    return 100;
  }else{
    return 1 + inputNumber;
  }
};
