import {userLongin} from './booleans';

describe('Proof of Boolean', () => {
  it('Validate is true', () => {
    const resp = userLongin();
    expect(resp).not.toBeFalse();
  });
});
