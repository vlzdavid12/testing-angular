import {message} from './string';

describe('Proof of the string', () => {
  it('Must return in string', () => {
    const resp = message('David');
    expect(typeof resp ).toBe('string');
  });
})

describe('Proof of the var name', () => {
  it('Must return var input name', () => {
    const name = 'Juan';
    const resp = message(name);
    expect( resp ).toContain(name);
  });
})
